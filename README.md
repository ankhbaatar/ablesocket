# Able 1.0 chat server
Chat server нь nodeJS socket.io ашиглаж хийсэн
## эхэлцгээе (get started)
```
$ git clone git@web.able.mn:NodeJS/AbleSocket.git
$ cd AbleSocket
$ npm install
$ npm install -g pm2
```
git ignore хийсэн тохиргооны файл config.js-ыг үүсгэнэ.
```
$ touch config.js
```
config.js файл дотроо доорхи кодыг хуулаад
зохих тохиргоонуудыг хийж өгнө
```javascript
var fs = require('fs');
var ssl ={
// 	secureProtocol: 'SSLv23_method',
// 	secureOptions: require('constants').SSL_OP_NO_SSLv3,
// 	ca: fs.readFileSync('/etc/httpd/ssl/EV_CA_Bundle.crt'),
	key: fs.readFileSync('./ssl/file.pem'),
	cert: fs.readFileSync('./ssl/file.crt'),
// 	passphrase: 'password'
};
var port =normalizePort(process.env.PORT || '8000');
var mysql ={
	host: "192.168.10.6",
	port: 3306,
	user: "client",
	password: "AbleSoft2016",
};
var redis ={
	host: "localhost",
	port: 6379
};
var mongo ={
	user: 'cloudUser',
	pass: 'cloudPass',
	host: 'localhost',
	// port: '27017',
	db: 'cloud',
	collections: ['notifies_i','getGenId']
};
var socket =[
	'websocket',
	'flashsocket',
	'htmlfile',
	'xhr-polling',
	'jsonp-polling',
	'polling'
];
module.exports = {
	port,
	// ssl,
	mysql,
	redis,
	mongo,
	socket,
}

/**
* Normalize a port into a number, string, or false.
*/
function normalizePort(val) {
	var port = parseInt(val, 10);

	if (isNaN(port)) {
		// named pipe
		return val;
	}

	if (port >= 0) {
		// port number
		return port;
	}

	return false;
}

```

## Хэрхэн ажиллуулах вэ? (How to run)
run development:
```
$ npm start
```
run production:
```
$ npm run serve
```

## Зохиогчийн эрх
@copyright 2017.