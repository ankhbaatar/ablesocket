'use strict';
var debug = require('debug')('root');
var config = require('./config');
debug(config);


// express init
var app = require('express')();
var bodyParser = require('body-parser');
const cors = require('cors');
const SocketIO = require('socket.io');

app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);

const corsOptions = {
    origin: '*',
    credentials: true,
};
app.use(bodyParser.json());
app.use(cors(corsOptions));

// socket.io init
if (config.ssl) {
    var https = require('https');
    var server = https.createServer(config.ssl, app);
} else {
    var server = require('http').Server(app);
}

server.listen(config.port, function () {
    console.log('listening on ' + (config.ssl ? 'https://' : '') + '*:' + config.port);
});

const io = SocketIO(server, {
    cors: {
        origin: '*',
        methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE'],
        credentials: true,
    },
    pingTimeout: 30000,
});

// db connection
var redis = require('./module/redis');
var mysql = require('./module/mysql');
var mongo = require('./module/mongo');
var db = {
    redis: redis(config.redis),
    mysql: mysql(config.mysql),
    mongo: config.mongo ? mongo(config.mongo) : null,
};

//apis
require('./router')(app, db, io);

// starting socket.io
var auth = require('./module/auth');
var socket = require('./module/socket')(db, io);
// config)
// io.set("transports", config.socket);
io.set('transports', [
    'websocket',
    'flashsocket',
    'htmlfile',
    'xhr-polling',
    'jsonp-polling',
    'polling',
]);

const getUsers = db.redis.get('ableOnlineUsers');
console.log('ableOnlineUsers', getUsers);

// auth
io.use(auth);
// connection
io.on('connection', socket);
