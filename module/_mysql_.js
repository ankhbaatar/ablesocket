'use strict';
var debug = require('debug')('able:mysql');
var mysql = require('promise-mysql');

var config = require('../config').mysql;

mysql.cloud = mysql.createPool(Object.assign({},config,{
	database: (config.cloudDB) ? config.cloudDB : 'cloud',
	connectionLimit : config.connectionLimit * 2,
}));
mysql.cloud_hr = mysql.createPool(Object.assign({},config,{
	database: (config.cloudHRDB) ? config.cloudHRDB : 'cloud_hr',
}));
module.exports = mysql;
