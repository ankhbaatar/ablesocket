'use strict';
var debug = require('debug')('auth');

module.exports =(socket, next) => {
	console.log("\n\n");
	var jwt = require('jsonwebtoken');
	var cookie =parseCookie(socket.handshake);

	if(!socket.handshake.query || !socket.handshake.headers)  {
		next(new Error('A bad request')); //cookie transmitted
		console.log('error', 'a bad request');
		return false;
	}

	var accessToken =cookie.accessToken || socket.handshake.query.accessToken || socket.handshake.headers['x-access-token'];
	if(!accessToken)  {
		next(new Error('undefined token'));  //No session id transmitted.
		console.log('error', 'undefined accessToken');
		return false;
	}
	try {
		var tokenData = jwt.verify(accessToken, '@bl3Soft2010');
		if(!tokenData.myHID) {
			next(new Error('undefined myHID'));  //No session id transmitted.
			console.log('error', 'undefined myHID');
		}
		if(!tokenData.myComId) {
			next(new Error('undefined myComId'));  //No session id transmitted.
			console.log('error', 'undefined myComId');
		}
		if(!tokenData.key) {
			next(new Error('undefined key'));  //No session id transmitted.
			console.log('error', 'undefined key');
		}
		debug(tokenData);

		// accept connection
		socket.type = socket.handshake.query.type || 'web';
		socket.user = tokenData;
		socket.token = accessToken;
		console.log('successfuly connected user:'+ tokenData.myHID);
		next();
	}
	catch(err) {
		next(err);  //No session id transmitted.
		console.log(err.message);
		return false;
	}
}

function parseCookie(request)  {
	var list = {},
		rc = request.headers.cookie;

	rc && rc.split(';').forEach(function( cookie ) {
		var parts = cookie.split('=');
		list[parts.shift().trim()] = unescape(parts.join('='));
	});

	return list;
}
