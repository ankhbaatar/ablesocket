
var basestring = "AbleCloudSoftware2012";
function encode (tmp)  {
	try  {
		tmp = tmp.toString();
		var str = '', i = 0, bi = 0;
		for (; i < tmp.length; i += 1)  {
			str += (tmp.charCodeAt(i) + basestring.charCodeAt(bi)).toString(16) + ' ';
			bi += 1; if(bi == basestring.length -1) bi = 0;
		}
		return str;
	}
	catch (e)  {
		console.log("encode: " + e);
		return '';
	}
}

function decode (tmp)  {
	try  {
		tmp = tmp.toString();
		var arr = tmp.split(' '), str = '', i = 0, bi = 0;
		for (; i < arr.length; i += 1)  {
			str += String.fromCharCode( parseInt(arr[i], 16) - basestring.charCodeAt(bi));
			bi += 1; if(bi == basestring.length -1) bi = 0;
		}
		str = str.toString().substring(0,str.toString().length - 1);
		return str;
	}
	catch (e)  {
		console.log("decode: " + e);
		return '';
	}
}

module.exports ={
	encode,
	decode
}
