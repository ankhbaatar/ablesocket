'use strict';
var debug = require('debug')('mongo');
var mongojs = require('mongojs');

module.exports =config =>  {
	var connStr =(config.host ? (config.host + (config.port ? ':'+ config.port : '') +'/') : '') + config.db;
	if(config.user && config.pass && config.host)
	  	connStr =config.user +':'+ config.pass +'@'+ connStr;
	return () => {
		var options = {
			connectTimeoutMS: 5000,
			socketTimeoutMS: 5000
		}
	    var mongoDB =mongojs(connStr, config.collections, options);
		mongoDB.on('error',function(err) {
			console.log('mongo connection string:', connStr);
	    })
	    mongoDB.on('connect', function () {
			console.log('connection string:', connStr);
	        console.log('MongoDB connected');
	    })
	    return mongoDB;
	}
}
