'use strict';
var debug = require('debug')('mysql');
var mysql = require('promise-mysql');

module.exports = config => {
	mysql.cloud = mysql.createPool(Object.assign({},config,{
		database: 'cloud',
  		connectionLimit : config.connectionLimit * 2,
	}));
	mysql.cloud_hr = mysql.createPool(Object.assign({},config,{
		database: 'cloud_hr',
	}));
	return mysql;
}
