'use strict';
var debug = require('debug')('mysql');
var Mysql = require('mysql2');

module.exports = config => {
    // Test connection health before returning it to caller.
	// console.log('test');
    // if ((module.exports.connection) && (module.exports.connection._socket)
            // && (module.exports.connection._socket.readable)
            // && (module.exports.connection._socket.writable)) {
        // return module.exports.connection;
    // };
    // console.log((module.exports.connection ? "UNHEALTHY SQL2 CONNECTION; RE" : "") + "CONNECTING TO SQL2. "+ (new Date).toString());
    var connection = Mysql.createConnection(config);
    connection.connect(function(err) {
        if (err) {
            debug("SQL2 CONNECT ERROR: " + err +". "+ (new Date).toString());
        } else {
            debug("SQL2 CONNECT SUCCESSFUL. "+ (new Date).toString());
        }
    });
    connection.on("close", function (err) {
        debug("SQL2 CONNECTION CLOSED."+ (new Date).toString());
    });
    connection.on("error", function (err) {
        debug("SQL2 CONNECTION ERROR: " + err +". "+ (new Date).toString());
    });
    return connection;
};
