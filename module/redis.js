'use strict';
var debug = require('debug')('redis');
var promiseFactory = require('q').Promise;
// var redis = require('promise-redis')(promiseFactory);

var unserialize = require('php-unserialize').unserialize;
var serialize = require('php-serialize').serialize;

module.exports = (config) => {
	var redis = require('promise-redis')(promiseFactory);
	var client = redis.createClient(config);
    client.on("connect", function () {
		console.log("REDIS CONNECT SUCCESSFUL. "+ (new Date).toString());
    });
    client.on("error", function (err) {
        console.log("REDIS CONNECTION ERROR: " + err +". "+ (new Date).toString());
    });
    client.on("end", function () {
        console.log("REDIS CONNECTION CLOSED."+ (new Date).toString());
    });

	// redis.get funciton override
	(function (getFn) {
		client.get =function(key) {
			return getFn.call(client, key).then(res => {
				if(!res) return null;
				return unserialize(res).value;
			}).catch((err) => {
				console.log(err);
			})
		}
	})(client.get);

	// redis.set funciton override
	(function (setFn) {
		client.set =function(key, val, exp) {
			var t =time();
			exp = exp || 86400;
			val = JSON.stringify(val);
			val = JSON.parse(val);

			val =serialize({
				value: val,
				write_time: t,
				expired_in: exp,
				expired_time: t + exp
			});
			return setFn.call(client, key, val);
		}
	})(client.set);

	function time () {
	  return Math.floor(new Date().getTime() / 1000)
	}
	return client;
}
