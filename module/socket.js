'use strict';
const axios = require('axios');
var reConnectTime = 3 * 1000,
    debug = require('debug')('socket'),
    crypt = require('./crypt');

module.exports = (db, io) => {
    var mysql = db.mysql,
        redis = db.redis,
        mongoDB = db.mongo ? db.mongo() : undefined,
        reConnectTimeIds = {},
        onlineUsers = [];

    // asset functions
    io.sockets.clients = (room, filter) => {
        let sockets = [];
        try {
            if (!io.sockets.adapter.rooms[room]) return sockets;
            let socketObj = io.sockets.adapter.rooms[room].sockets;

            for (let id of Object.keys(socketObj)) {
                sockets.push(io.sockets.connected[id]);
            }
        } catch (e) {
            console.error('Error in io.sockets.clients:', `Non-existent room: ${room}`, e);
        }
        return sockets;
    }; //*/

    // online user registeration functions
    function writeOnlineUsersToRedis() {
        // console.log('onlineUsers', onlineUsers);
        redis
            .set('ableOnlineUsers', onlineUsers)
            .then(debug)
            .catch(err => {
                console.error('Error in addOnlineUser:', err);
            });
    }

    function addOnlineUser(userId) {
        if (onlineUsers.indexOf(userId) === -1) {
            if (userId == 5) return;
            onlineUsers.push(userId);
            writeOnlineUsersToRedis();
        }
    }

    function removeOnlineUser(userId) {
        var ind = onlineUsers.indexOf(userId);
        if (ind > -1) {
            onlineUsers.splice(ind, 1);
            writeOnlineUsersToRedis();
        }
    }

    function getUserInfo(userId) {
        // хэрэв redis cache-д хэрэлэгчийн мэдээлэл байвал авна
        // байхгүй бол mysql-ээс авна.
        // _able_worker_i
        return redis
            .get('_able_worker_i-' + userId)
            .then(res => {
                // debug('redis res:',res);
                if (!res) {
                    return mysql.cloud_hr
                        .getConnection()
                        .then(conn => {
                            var sql = 'SELECT * FROM ?? WHERE ?? = ?';
                            var para = ['_able_worker_i', 'id', userId];
                            sql = mysql.format(sql, para);
                            var result = conn.query(sql);
                            //conn.release();
                            mysql.cloud_hr.releaseConnection(conn);
                            return result;
                        })
                        .then(rows => {
                            redis.set('_able_worker_i-' + userId, rows[0]).then(debug);
                            // debug('mysql res:',rows[0]);
                            return rows[0];
                        })
                        .catch(err => {
                            console.error('Error in getUserInfo:', err);
                        });
                }
                return res;
            })
            .then(wRow => {
                var humanId = wRow['user_id'];
                // _user_i
                var uPromise = redis
                    .get('_user_i-' + userId)
                    .then(res => {
                        // debug('redis res:',res);
                        if (!res) {
                            return mysql.cloud
                                .getConnection()
                                .then(conn => {
                                    var sql = 'SELECT * FROM ?? WHERE ?? = ?';
                                    var para = ['_user_i', 'user_id', humanId];
                                    sql = mysql.format(sql, para);
                                    var result = conn.query(sql);
                                    mysql.cloud.releaseConnection(conn);
                                    //conn.release();
                                    return result;
                                })
                                .then(rows => {
                                    redis.set('_user_i-' + userId, rows[0]).then(debug);
                                    // debug('mysql res:',rows[0]);
                                    return rows[0];
                                })
                                .catch(err => {
                                    console.error('Error in getUserInfo:', err);
                                });
                        }
                        return res;
                    })
                    .catch(err => {
                        console.error('Error in getUserInfo:', err);
                    });
                // _able_human_i
                var hPromise = redis
                    .get('_able_human_i-' + userId)
                    .then(res => {
                        // debug('redis res:',res);
                        if (!res) {
                            return mysql.cloud_hr
                                .getConnection()
                                .then(conn => {
                                    var sql = 'SELECT * FROM ?? WHERE ?? = ?';
                                    var para = ['_able_human_i', 'id', humanId];
                                    sql = mysql.format(sql, para);
                                    var result = conn.query(sql);
                                    mysql.cloud_hr.releaseConnection(conn);
                                    //conn.release();
                                    return result;
                                })
                                .then(rows => {
                                    redis.set('_able_human_i-' + userId, rows[0]).then(debug);
                                    // debug('mysql res:',rows[0]);
                                    return rows[0];
                                })
                                .catch(err => {
                                    console.error('Error in getUserInfo:', err);
                                });
                        }
                        return res;
                    })
                    .catch(err => {
                        console.error('Error in getUserInfo:', err);
                    });

                return Promise.all([uPromise, hPromise]).then(value => {
                    return {
                        wRow: wRow,
                        uRow: value[0],
                        hRow: value[1],
                    };
                });
            })
            .catch(err => {
                console.error('Error in getUserInfo:', err);
            });
    }

    function emitToOthers(sender, type, data) {
        if (typeof sender !== 'object') return;
        if (typeof type !== 'string' || type == '') return;
        var comIds = [sender.comId];

        // emit to child
        redis
            .get('tbl_messenger_option-' + sender.comId)
            .then(res => {
                if (!res) {
                    return mysql.cloud
                        .getConnection()
                        .then(conn => {
                            var sql = 'SELECT * FROM ?? WHERE ?? = ?';
                            var para = ['tbl_messenger_option', 'com_id', sender.comId];
                            sql = mysql.format(sql, para);
                            var result = conn.query(sql);
                            //conn.release();
                            mysql.cloud.releaseConnection(conn);
                            return result;
                        })
                        .then(rows => {
                            rows.forEach(row => {
                                var childComId = row.child_id;
                                if (childComId == 0) return;
                                if (comIds.indexOf(childComId) > -1) return;
                                comIds.push(childComId);
                            });
                            debug('mysql res:', comIds);
                            redis.set('tbl_messenger_option-' + sender.comId, comIds);
                            return comIds;
                        })
                        .catch(err => {
                            console.error('Error in emitToOthers:', err);
                        });
                }
                return res;
            })
            .then(res => {
                // debug('redis res:', res);
                Object.keys(res).forEach(ind => {
                    io.to('com' + res[ind]).emit(type, data);
                });
                return mysql.cloud.getConnection();
            })
            // emit to recent
            .then(conn => {
                var sql = 'SELECT * FROM ?? WHERE ?? = ? OR ?? = ?';
                var para = [
                    'tbl_messenger_recent_users',
                    'to_id',
                    sender.userId,
                    'from_id',
                    sender.userId,
                ];
                sql = mysql.format(sql, para);
                var result = conn.query(sql);
                //conn.release();
                mysql.cloud.releaseConnection(conn);
                return result;
            })
            .then(rows => {
                rows.forEach(row => {
                    var recentId = row.to_id == sender.userId ? row.from_id : row.to_id;
                    var curSocket = io.sockets.clients(recentId);
                    if (curSocket.length == 0) return;
                    if (comIds.indexOf(curSocket[0].comId) > -1) return;
                    debug('mysql res:', row);
                    io.to(recentId).emit(type, data);
                });
            })
            .catch(err => {
                console.error('Error in emitToOthers:', err);
            });
    }

    function isDesktopLoggedIn(userId) {
        var sockets = io.sockets.clients(userId);
        for (var i = 0; i < sockets.length; i++) {
            if (sockets[i].type === 'desktop') {
                return true;
            }
        }
        return false;
    }

    function userIdsToArray(data) {
        if (typeof data.users == 'object') {
            // Хэрэглэгчдийн жагсаалт
            var userIds = [];
            Object.keys(data.users).forEach(i => {
                if (typeof data.users[i] == 'string') data.users[i] = parseInt(data.users[i]);
                if (typeof data.users[i] != 'number') return;
                if (data.users[i] < 1) return;
                userIds.push(data.users[i]);
            });
            data.users = userIds;
            return userIds;
        } else {
            if (typeof data.comId != 'number' || data.comId < 1) {
                console.log('cant insert notify into db. undefined data.comId');
                return [];
            }

            return mysql.cloud_hr
                .getConnection()
                .then(conn => {
                    var qry = 'SELECT id FROM _able_worker_i ';
                    qry += 'WHERE `worker_type`=1 AND com_id=' + mysql.escape(data.comId);
                    var result = conn.query(qry);
                    //conn.release();
                    mysql.cloud_hr.releaseConnection(conn);
                    return result;
                })
                .then(rows => {
                    return rows.map(row => row.id);
                })
                .catch(err => {
                    console.error('Error in userIdsToArray:', qry, err);
                });
        }
    }

    function getRootComId(dbKey, rootComId) {
        if (!rootComId) rootComId = 0;
        return redis.get('comRow' + rootComId).then(res => {
            if (res) return rootComId;

            return mysql.cloud
                .getConnection()
                .then(conn => {
                    var sql = 'SELECT * FROM ?? WHERE ?? = ?';
                    var para = ['companies_i', 'db_key', dbKey];
                    sql = mysql.format(sql, para);
                    var result = conn.query(sql);
                    //conn.release();
                    mysql.cloud.releaseConnection(conn);
                    return result;
                })
                .then(rows => {
                    if (rows.length == 0) return rootComId;
                    rootComId = rows[0].com_id;
                    redis.set('comRow' + rootComId, rows[0]);
                    return rootComId;
                })
                .catch(err => {
                    console.error('Error in getRootComId:', err);
                });
        });
    }

    // message functions
    function sendMsg(time, userId, toId, message, file, callBack) {
        var msg = crypt.encode(message),
            connection,
            sql,
            para,
            dir = file ? file.dir : '';

        mysql.cloud
            .getConnection()
            .then(conn => {
                connection = conn;
                para = [time, userId, toId, msg, dir];
                sql = mysql.format(
                    'INSERT INTO `tbl_messenger_offline` ' + "VALUES(?,?,?,?,NOW(),?,'',0,1,1,0)",
                    para
                );
                var result = connection.query(sql);

                // active log write
                // var log_data = JSON.stringify({
                // 	ajax: {
                // 		program: 'chat',
                // 		action: 'sendMsg'
                // 	},
                // 	user: {
                // 		userId: userId
                // 	},
                // 	body: {
                // 		receiver: toId,
                // 		message: message
                // 	}
                // });
                // Client.send(log_data, 0, log_data.length, UDPORT, UDHOST);

                callBack(toId);
                return result;
            })
            .then(res => {
                debug(res);
                sql =
                    'SELECT * FROM `tbl_messenger_recent_users` ' +
                    'WHERE `from_id` IN ( ?,? ) AND `to_id` IN ( ?,? )';
                para = [userId, toId, userId, toId];
                sql = mysql.format(sql, para);
                // debug(sql);
                return connection.query(sql);
            })
            .then(rows => {
                var now = parseInt(new Date().getTime() / 1000);
                var result;
                if (rows.length == 0) {
                    sql = 'INSERT INTO ?? ( ??,??,?? ) VALUES( ?,?,?)';
                    para = [
                        'tbl_messenger_recent_users',
                        'from_id',
                        'to_id',
                        'date',
                        userId,
                        toId,
                        now,
                    ];
                    sql = mysql.format(sql, para);
                    result = connection.query(sql);
                } else if (rows.length == 1) {
                    sql = 'UPDATE ?? SET ?? = ? WHERE ?? IN ( ?,? ) AND ?? IN ( ?,? )';
                    para = [
                        'tbl_messenger_recent_users',
                        'date',
                        now,
                        'from_id',
                        userId,
                        toId,
                        'to_id',
                        userId,
                        toId,
                    ];
                    sql = mysql.format(sql, para);
                    result = connection.query(sql);
                }
                //connection.end();
                mysql.cloud.releaseConnection(connection);
                return result;
            })
            .catch(err => {
                if (connection) mysql.cloud.releaseConnection(connection); //connection.end();
                console.error('Error in sendMsg:', err);
            });
    }

    function sendCall(time, userId, toId, message, file, callBack, data) {
        console.log('sendCall message');
        console.log(message);
        console.log(data);

        var msg,
            connection,
            sql,
            para,
            dir = file ? file.dir : '',
            action = '';

        if (message == '~call~') action = JSON.stringify({ calling: data });
        else action = data;

        msg = message;
        mysql.cloud
            .getConnection()
            .then(conn => {
                connection = conn;
                para = [time, userId, toId, msg, dir, action];

                if (checkCallMsg(message, 2)) {
                    var result = selectRow(message, connection, mysql, toId, userId);
                } else {
                    //~call~ engiin chat meteer ajillana duudlaga irj bgaa
                    sql = mysql.format(
                        'INSERT INTO `tbl_messenger_offline` ' +
                            'VALUES(?,?,?,?,NOW(),?,?,0,1,1,0)',
                        para
                    );
                    var result = connection.query(sql);
                }
                callBack(toId);
                return result;
            })
            .then(res => {
                console.log('res debug');
                debug(res);
                console.log(res);

                if (checkCallMsg(message, 2)) {
                    // ene bol call
                    var calldatas = JSON.parse(res[0].call_datas);
                    var id = res[0].id_stamp;
                    var answer = {
                        calling: calldatas['calling'],
                        '~doAnswer~': calldatas['~doAnswer~'],
                        incomeEnd: calldatas['incomeEnd'],
                        incomeForce: calldatas['incomeForce'],
                        outcomeEnd: calldatas['outcomeEnd'],
                        [message]: action,
                    };

                    if (message == '~missed~') {
                        answer = JSON.stringify(answer);
                        sql = 'UPDATE ?? SET ?? = ?';

                        para = ['tbl_messenger_history', 'call_datas', answer, 'id_stamp', id]; //online bol
                        //para = ['tbl_messenger_offline', 'call_datas', answer , 'id_stamp', id];    //offline bol
                        sql = mysql.format(sql, para);
                        connection.query(sql);

                        console.log('result missed updated');
                        // console.log(result);
                    } else {
                        answer = JSON.stringify(answer);

                        sql = 'UPDATE ?? SET ?? = ? WHERE ?? = ?';
                        para = ['tbl_messenger_history', 'call_datas', answer, 'id_stamp', id];
                        sql = mysql.format(sql, para);
                        connection.query(sql);
                    }
                }

                sql =
                    'SELECT * FROM `tbl_messenger_recent_users` ' +
                    'WHERE `from_id` IN ( ?,? ) AND `to_id` IN ( ?,? )';
                para = [userId, toId, userId, toId];
                sql = mysql.format(sql, para);
                // debug(sql);
                return connection.query(sql);
            })
            .then(rows => {
                var now = parseInt(new Date().getTime() / 1000);
                var result;
                if (rows.length == 0) {
                    sql = 'INSERT INTO ?? ( ??,??,?? ) VALUES( ?,?,?)';
                    para = [
                        'tbl_messenger_recent_users',
                        'from_id',
                        'to_id',
                        'date',
                        userId,
                        toId,
                        now,
                    ];
                    sql = mysql.format(sql, para);
                    result = connection.query(sql);
                } else if (rows.length == 1) {
                    sql = 'UPDATE ?? SET ?? = ? WHERE ?? IN ( ?,? ) AND ?? IN ( ?,? )';
                    para = [
                        'tbl_messenger_recent_users',
                        'date',
                        now,
                        'from_id',
                        userId,
                        toId,
                        'to_id',
                        userId,
                        toId,
                    ];
                    sql = mysql.format(sql, para);
                    result = connection.query(sql);
                }
                //connection.end();
                mysql.cloud.releaseConnection(connection);
                return result;
            })
            .catch(err => {
                if (connection) mysql.cloud.releaseConnection(connection); //connection.end();
                console.error('Error in sendMsg:', err);
            });
    }

    // notify functions
    function replaceNotifyEnd(txt) {
        if (txt == '') return txt;
        if (txt.search('шилжиж ирлээ') > 0) return txt.replace('шилжиж ирлээ', 'шилжүүллээ');
        if (txt.search('ирлээ') > 0) return txt.replace('ирлээ', 'илгээлээ');
        if (txt.search('шинэчлэгдлээ') > 0) return txt.replace('шинэчлэгдлээ', 'шинэчиллээ');
        if (txt.search('нэмэгдлээ') > 0) return txt.replace('нэмэгдлээ', 'нэмлээ');
        if (txt.search('хянагдлаа') > 0) return txt.replace('хянагдлаа', 'хяналаа');
        if (txt.search('гүйцэтгэгдлээ') > 0) return txt.replace('гүйцэтгэгдлээ', 'гүйцэтгэлээ');
        return txt;
    }

    function insertNotifyToMysql(data, dbKey) {
        var qry =null;
        var fields =
                '`item_id`,`content`,`headText`,`title`,`pro`,`mod`,`type`,`url`,`sender_id`,`sender_name`,`sender_icon`,`sent_date`',
            value =
                mysql.escape(data.itemId) +
                ',' +
                mysql.escape(data.content) +
                ',' +
                mysql.escape(replaceNotifyEnd(data.headText)) +
                ',' +
                mysql.escape(data.title) +
                ',' +
                mysql.escape(data.pro) +
                ',' +
                mysql.escape(data.mod) +
                ',' +
                mysql.escape(data.type) +
                ',' +
                mysql.escape(data.url) +
                ',' +
                mysql.escape(data.userId) +
                ',' +
                mysql.escape(data.userName) +
                ',' +
                mysql.escape(data.userIcon) +
                ',UNIX_TIMESTAMP()';
        if(data.pro == 'public' || data.pro =='mail' || (data.pro == 'docs' && (data.mod =='complain' || data.mod =='received')) || (data.comId ==2 && data.pro =='time_access' && data.mod =='timeRequest')) {
            fields += ',`actionKey`';
            value += ',' + mysql.escape(data.actionKey);
        }

        return mysql.cloud
            .getConnection()
            .then(conn => {
                qry =
                    'INSERT INTO `' +
                    dbKey +
                    '`.`notify_i` ' +
                    '(' +
                    fields +
                    ')' +
                    'VALUES(' +
                    value +
                    ')';
                var result = conn.query(qry);
                //conn.release();
                mysql.cloud.releaseConnection(conn);
                return result;
            })
            .then(res => {
                if (data.pro == 'public' || data.pro =='mail' || (data.pro == 'docs' && (data.mod =='complain' || data.mod =='received')) || (data.comId ==2 && data.pro =='time_access' && data.mod =='timeRequest')) {
                    // Олон нийт дээр хамааралтай notify үйлдэл хийх бүрт posts table рүү insert хийнэ
                    let postData = {
                        authorId: data.userId,
                        notifyId: res.insertId,
                        comId: data.comId,
                        pro: data.pro,
                        module: data.mod,
                        body: data.body,
                        actionKey: data.actionKey,
                        dbKey: dbKey
                    };

                    if(data.hasOwnProperty('actionId'))  postData['actionId'] =data.actionId;

                    if (data.actionKey == 'addImage') postData['itemId'] = data.content;
                    else postData['itemId'] = data.itemId;

                    var momentsDomain ='';
                    // var momentsDomain ='localhost:5003';
                    if(data.server =='able')  momentsDomain ='moments.able.mn';
                    if(data.server =='intranet')  momentsDomain ='intranet.gov.mn:2003';
                    if(data.server =='ubtz')  momentsDomain ='ubtzmoments.able.mn';
                    if(data.server =='ndc')  momentsDomain ='ndcmoments.able.mn';
                    if(data.server =='mcud')  momentsDomain ='mcudmoments.able.mn';
                    if(data.server =='ubedn')  momentsDomain ='able.tog.mn:2003';
                    if(data.server =='mojha')  momentsDomain ='intranet.mojha.gov.mn:2003';
                    if(data.server =='audit')  momentsDomain ='moments.audit.mn';
                    if(data.server =='transco')  momentsDomain ='moments.transco.mn';

                    if(momentsDomain)  {
                        axios
                        .post('https://'+ momentsDomain +'/api/post', {
                        // .post('http://localhost:5003/api/post', {
                            // post table рүү insert хийх moments ийн api
                            data: postData,
                        })
                        .then(response => {
                            return response.data;
                        })
                        .then(responseJson => {
                            console.log('responseJson', responseJson.data);
                        })
                        .catch(error => {
                            console.error('error', error.data);
                        });
                    }
                }
                return res.insertId;
            })
            .catch(err => console.error('Error in insertNotifyToMysql:', qry, err));
    }

    function recentUsersUpdate(dbKey, userId, pro, mod, recenIds) {
        var qry, connection;
        if (recenIds.length == 0) return;
        mysql.cloud
            .getConnection()
            .then(conn => {
                connection = conn;
                qry =
                    'DELETE FROM `' +
                    dbKey +
                    '`.`recent_user_byuser_k` WHERE `holder_id`=' +
                    mysql.escape(userId) +
                    " AND `pro`='" +
                    pro +
                    "' AND `mod`='" +
                    mod +
                    "'";
                return connection.query(qry);
            })
            .then(res => {
                debug(res);
                qry =
                    'INSERT INTO `' +
                    dbKey +
                    '`.`recent_user_byuser_k`(`holder_id`,`user_id`,`pro`,`mod`,`window`,`input`) ' +
                    'SELECT ' +
                    mysql.escape(userId) +
                    ",`id`,'mass','mass','massMsg','userAccess' FROM " +
                    '`cloud_hr`.`_able_worker_i` WHERE `id` IN (' +
                    recenIds.join(',') +
                    ')';
                // debug(qry);
                return connection.query(qry);
            })
            //.then(() => connection.end())
            .then(() => mysql.cloud.releaseConnection(connection))
            .then(res =>
                debug(
                    res.affectedRows + ' users inserted into ' + pro + ':' + mod + ' recent users.'
                )
            )
            .catch(err => {
                if (connection) mysql.cloud.releaseConnection(connection); //connection.end();
                console.error('Error in recentUsersUpdate:', qry, err);
            });
    }

    // mass message functions
    function sendMass(users, data, socket) {
        if (typeof users != 'object') return;
        var msg = '<font color="blue">' + data.title + '</font></br>' + data.content;

        users.forEach(uId => {
            if (uId == 0 || uId == socket.userId) return;
            console.log('sent mass: ' + uId);
            sendMsg(data.notifyId, socket.userId, uId, msg, '', function (toId) {
                if (isDesktopLoggedIn(toId)) {
                    io.to('desktop' + toId).emit('notify', data);
                    io.to('web' + toId).emit('receive', socket.userId, { close: true });
                } else {
                    io.to(toId).emit('receive', socket.userId, { id: data.notifyId, message: msg });
                }
            });
        });
    }

    function sendWindowVideo(users, data, socket) {
        console.log('sendWindowVideo', users);
        users.forEach(uId => {
            //if (uId == 0 || uId == socket.userId) return;
            var dt = new Date();
            data.id = dt.getTime();
            io.to(uId).emit('window', socket.userId, {
                response: 'unSeen',
                msg: data.message,
                data: data,
                send_id: data.send_id,
            });
        });
    }

    function sendMassToCom(data, socket, comIds) {
        return mysql.cloud_hr
            .getConnection()
            .then(conn => {
                var sql =
                    'SELECT `id` FROM `_able_worker_i`' +
                    ' WHERE `worker_type`=1 AND `com_id` IN (' +
                    comIds +
                    ')';
                var result = conn.query(sql);
                //conn.release();
                mysql.cloud_hr.releaseConnection(conn);
                return result;
            })
            .then(rows => {
                if (rows.length < 1) return true;
                var userIds = rows.map(row => row.id);
                sendMass(userIds, data, socket);
                return true;
            })
            .catch(err => {
                console.error('Error in sendMassToCom:', err);
            });
    }

    function sendMassWindow(data, socket, comIds) {
        return mysql.cloud_hr
            .getConnection()
            .then(conn => {
                var sql =
                    'SELECT `id` FROM `_able_worker_i`' +
                    ' WHERE `worker_type`=1 AND `com_id` IN (' +
                    comIds +
                    ')';
                var result = conn.query(sql);
                //conn.release();
                mysql.cloud_hr.releaseConnection(conn);
                return result;
            })
            .then(rows => {
                //console.log('sendMassWindow', comIds);
                if (rows.length < 1) return true;
                var userIds = rows.map(row => row.id);
                sendWindowVideo(userIds, data, socket);
                return true;
            })
            .catch(err => {
                console.error('Error in sendMassToWindow:', err);
            });
    };

    function sendMassToAll(data, socket, comIds) {
        debug(comIds);
        sendMassToCom(data, socket, comIds);

        return mysql.cloud
            .getConnection()
            .then(conn => {
                var sql =
                    'SELECT `id`,`type` FROM `_tree_i` ' +
                    'WHERE `type` IN (2,3) AND `mid` IN (' +
                    comIds +
                    ')';
                var result = conn.query(sql);
                //	conn.release();
                mysql.cloud.releaseConnection(conn);
                return result;
            })
            .then(rows => {
                if (rows.length < 1) return true;
                var childIds = rows.map(row => mysql.escape(row.id));
                return sendMassToAll(data, socket, childIds.join(','));
            })
            .catch(err => {
                console.error('Error in sendMassToAll:', err);
            });
    }

    function checkCallMsg(message, type) {
        var isCall = false;
        if (type == 1) {
            if (message == '~call~') isCall = true;
        }

        if (type == 2) {
            if (message != '~call~') isCall = true;
        }

        return isCall;
    }

    function selectRow(message, connection, mysql, toId, userId) {
        var selectRow = '',
            callerId = '',
            sql = '';

        if (
            message == '~doAnswer~' ||
            message == '~force~' ||
            message == '~end~' ||
            message == 'incomeEnd' ||
            message == 'incomeForce'
        ) {
            callerId = [toId, userId];
            sql = mysql.format(
                'SELECT * FROM `tbl_messenger_history`' +
                    " WHERE `message`='~call~' AND `from_id` IN ( ?,? ) ORDER BY `id_stamp` DESC LIMIT 1"
            );
            selectRow = connection.query(sql, callerId);
        } else if (message == '~missed~') {
            var para = ['~call~'];
            // sql = mysql.format(   //offline bol
            // 	"SELECT * FROM `tbl_messenger_offline`"+
            // 	" WHERE `message`=? ORDER BY `id_stamp` DESC LIMIT 1");
            // 	selectRow = connection.query(sql, para);

            sql = mysql.format(
                //online bol
                'SELECT * FROM `tbl_messenger_history`' +
                    ' WHERE `message`=? ORDER BY `id_stamp` DESC LIMIT 1'
            );
            selectRow = connection.query(sql, para);

            console.log('selectRow missed SELECT');
            console.log(selectRow);
        }

        return selectRow;
    }

    function encStr(str) {
        let value = new Buffer.from(str);

        let value1 = value.toString('base64');
        let len = value1.length;
        let a = Math.ceil(len / 3);
        let b = Math.ceil(len / 4);
        let cut1 = value1.substr(a, 1);
        let cut2 = value1.substr(b, 1);

        let paste1 = replaceAt(value1, b, cut1);
        let paste2 = replaceAt(paste1, a, cut2);
        return paste2;
    }

    function replaceAt(str, index, ch) {
        return str.replace(/./g, (c, i) => (i == index ? ch : c));
    }

    return socket => {
        socket.dbKey = socket.user.key;
        socket.userId = socket.user.myHID;
        socket.comId = socket.user.myComId;
        socket.isDesktop = socket.type === 'desktop';
        socket.isMobile = socket.type === 'mobile';
        socket.isWeb = socket.type === 'web';

        socket.join('com' + socket.comId);
        socket.join(socket.userId);
        socket.join(socket.type + '' + socket.userId);

        // register online user
        console.log('Connected user id:' + socket.userId + '; type:' + socket.type);
        clearTimeout(reConnectTimeIds[socket.userId]);
        reConnectTimeIds[socket.userId] = 0;

        addOnlineUser(socket.userId);
        getUserInfo(socket.userId)
            .then(res => {
                if (socket.userId == 5) return;
                emitToOthers(socket, 'addNewUser', {
                    id: socket.userId,
                    system_name: res.wRow.system_name,
                    app_name: res.wRow.app_name,
                    com_id: socket.comId,
                    position: res.wRow.position,
                    status: '',
                    user_icon: res.uRow.user_icon,
                    usericon_path: encStr('userIcon/' + res.uRow.user_icon),
                    onlineUsers: io.sockets.clients('com' + socket.comId).reduce((res, sock) => {
                        if (!res.includes(sock.userId)) res.push(sock.userId);
                        return res;
                    }, []).length,
                });
            })
            .catch(err => {
                console.error('Error in connection:', err);
            });

        // remove online user
        socket.on('disconnect', () => {
            var userId = socket.userId;
            var connectionCnt = io.sockets.clients(userId).length;
            console.log('\n\n');
            console.log(
                'Disconnected user id:' +
                    userId +
                    '; type:' +
                    socket.type +
                    '; connectionCnt:' +
                    connectionCnt
            );
            if (connectionCnt < 1) {
                function removeUser() {
                    console.log('Remove online user id:' + userId);
                    if (!io.sockets.clients(userId).length) {
                        removeOnlineUser(userId);
                        emitToOthers(socket, 'removeUser', userId);
                    }
                }
                if (socket.isDesktop) removeUser();
                else reConnectTimeIds[userId] = setTimeout(removeUser, reConnectTime);
            }
        });

        // listening
        socket.on('send', (to, data) => {
            if (typeof data === 'string') {
                data = JSON.parse(data);
            }
            try {
                if (typeof data === 'string') {
                    data = JSON.parse(data);
                }
                if (!to || !data) return true;
                if (socket.userId < 1) return;
                // write offline
                if (typeof data.message == 'string') {
                    var dt = new Date();
                    console.log(data, dt.toString());
                    data.id = dt.getTime();
                    sendMsg(data.id, socket.userId, to, data.message, data.file, function () {
                        // өөрлүүгээ
                        io.to(socket.userId).emit('receive', to, {
                            response: 'unSeen',
                            msg: data.message,
                            send_id: data.send_id,
                            id: data.id,
                            file: data.file,
                        });
                        // нөгөө хүнрүүгээ
                        console.log('send message from ' + socket.userId + ' to ' + to);
                        if (isDesktopLoggedIn(to)) {
                            io.to('mobile' + to).emit('receive', socket.userId, data);
                            io.to('desktop' + to).emit('receive', socket.userId, data);
                            io.to('web' + to).emit('receive', socket.userId, { close: true });
                        } else {
                            io.to(to).emit('receive', socket.userId, data);
                        }
                    });
                    return;
                }

                var connection, sql, para;
                // change offline status
                if (data.response) {
                    mysql.cloud
                        .getConnection()
                        .then(conn => {
                            connection = conn;
                            sql = 'INSERT INTO ?? SELECT * FROM ?? WHERE ?? = ?';
                            para = [
                                'tbl_messenger_history',
                                'tbl_messenger_offline',
                                'to_id',
                                socket.userId,
                            ];
                            sql = mysql.format(sql, para);
                            return connection.query(sql);
                        })
                        .then(res => {
                            debug(res);
                            sql = 'DELETE FROM ?? WHERE ?? = ?';
                            para = ['tbl_messenger_offline', 'to_id', socket.userId];
                            sql = mysql.format(sql, para);
                            return connection.query(sql);
                        })
                        .then(res => {
                            debug(res);
                            var result = {};
                            if (data.response == 'seen') {
                                sql =
                                    'UPDATE ?? FORCE INDEX(??) SET ?? = 0 WHERE ?? = 1 AND ?? = ?';
                                para = [
                                    'tbl_messenger_history',
                                    'to_id',
                                    'is_new',
                                    'is_new',
                                    'to_id',
                                    socket.userId,
                                ];
                                sql = mysql.format(sql, para);
                                result = connection.query(sql);
                            }
                            //connection.end();
                            mysql.cloud.releaseConnection(connection);
                            return result;
                        })
                        .catch(err => {
                            if (connection) mysql.cloud.releaseConnection(connection); //connection.end();
                            console.error("Error in socket.on('send'):", err);
                        });
                    return;
                } else if (data.imSeen) {
                    mysql.cloud
                        .getConnection()
                        .then(conn => {
                            sql = 'UPDATE ?? FORCE INDEX(??) SET ?? = 0 WHERE ?? = 1 AND ?? = ?';
                            para = [
                                'tbl_messenger_history',
                                'to_id',
                                'is_new',
                                'is_new',
                                'to_id',
                                socket.userId,
                            ];
                            sql = mysql.format(sql, para);
                            // console.log(sql);
                            var result = conn.query(sql);
                            //conn.release();
                            mysql.cloud.releaseConnection(conn);
                            return result;
                        })
                        .catch(err => {
                            console.error("Error in socket.on('send'):", err);
                        });
                }

                if (isDesktopLoggedIn(to)) {
                    io.to('desktop' + to).emit('receive', socket.userId, data);
                    io.to('web' + to).emit('receive', socket.userId, { close: true });
                } else {
                    io.to(to).emit('receive', socket.userId, data);
                }
            } catch (err) {
                console.log(err);
            }
        });

        socket.on('window', (to, data) => {
            console.log('data', data);
            console.log('data', typeof data);
            if (typeof data === 'string') {
                data = JSON.parse(data);
            }
            try {
                if (!to || !data) return true;
                if (socket.userId < 1) return;
                // write offline
                if (typeof data.message == 'string') {
                    var dt = new Date();
                    console.log(data, dt.toString());
                    data.id = dt.getTime();
                    console.log('orov');
                    io.to(to).emit('window', socket.userId, {
                        response: 'unSeen',
                        msg: data.message,
                        data: data,
                        send_id: data.send_id,
                    });

                    return;
                }
            } catch (err) {
                console.log(err);
            }
        });

        socket.on('alertNotify', users => {
            console.log('aletrt', users);
            users.forEach(uId => {
                if (uId == 0 || uId == socket.userId) return;
                io.to(uId).emit('alertNotify', uId, {
                    response: 'success',
                });
            });
        });

        socket.on('closeWindow', data => {
            console.log('hi socket', data);
            if (data.userId == 0) return;
            io.to(data.userId).emit('closeWindow', data.userId, {
                userId: data.userId,
                itemId: data.itemId
            });
        });

        socket.on('sendCall', (to, data) => {
            try {
                console.log(' socket on sendCall');
                console.log(data);

                if (!to || !data) return true;
                if (socket.userId < 1) return;
                // write offline
                if (typeof data.message == 'string') {
                    var dt = new Date();
                    data.id = dt.getTime();

                    sendCall(
                        data.id,
                        socket.userId,
                        to,
                        data.message,
                        data.file,
                        function () {
                            // өөрлүүгээ
                            io.to(socket.userId).emit('receiveCall', to, {
                                response: 'unSeen',
                                msg: data.message,
                                send_id: data.send_id,
                                duration: data.duration,
                                id: data.id,
                            });

                            // нөгөө хүнрүүгээ
                            io.to(to).emit('receiveCall', socket.userId, data);
                        },
                        data
                    );
                    return;
                }

                var connection, sql, para;
                // change offline status
                if (data.response) {
                    // ene zowhon online users deer  //chat.js =1647
                    mysql.cloud
                        .getConnection()
                        .then(conn => {
                            connection = conn;
                            sql = 'INSERT INTO ?? SELECT * FROM ?? WHERE ?? = ?';
                            para = [
                                'tbl_messenger_history',
                                'tbl_messenger_offline',
                                'to_id',
                                socket.userId,
                            ];
                            sql = mysql.format(sql, para);
                            return connection.query(sql);
                        })
                        .then(res => {
                            debug(res);

                            sql = 'DELETE FROM ?? WHERE ?? = ?';
                            para = ['tbl_messenger_offline', 'to_id', socket.userId];
                            sql = mysql.format(sql, para);
                            return connection.query(sql);
                        })
                        .then(res => {
                            debug(res);
                            var result = {};
                            if (data.response == 'seen') {
                                sql =
                                    'UPDATE ?? FORCE INDEX(??) SET ?? = 0 WHERE ?? = 1 AND ?? = ?';
                                para = [
                                    'tbl_messenger_history',
                                    'to_id',
                                    'is_new',
                                    'is_new',
                                    'to_id',
                                    socket.userId,
                                ];
                                sql = mysql.format(sql, para);
                                result = connection.query(sql);
                            }
                            //connection.end();
                            mysql.cloud.releaseConnection(connection);
                            return result;
                        })
                        .catch(err => {
                            if (connection) mysql.cloud.releaseConnection(connection); //connection.end();
                            console.error("Error in socket.on('send'):", err);
                        });
                    return;
                } else if (data.imSeen) {
                    //offline humuus online bolon orj irehed
                    mysql.cloud
                        .getConnection()
                        .then(conn => {
                            sql = 'UPDATE ?? FORCE INDEX(??) SET ?? = 0 WHERE ?? = 1 AND ?? = ?';
                            para = [
                                'tbl_messenger_history',
                                'to_id',
                                'is_new',
                                'is_new',
                                'to_id',
                                socket.userId,
                            ];
                            sql = mysql.format(sql, para);
                            var result = conn.query(sql);
                            //conn.release();
                            mysql.cloud.releaseConnection(conn);
                            return result;
                        })
                        .catch(err => {
                            console.error("Error in socket.on('send'):", err);
                        });
                }

                io.to(to).emit('receiveCall', socket.userId, data);
            } catch (err) {
                console.log(err);
            }
        });

        socket.on('notify', data => {
            if (!data) {
                console.log('notify data undefined');
                return true;
            }
            var qry;
            var nowDate = Date.now();
            var userIds = userIdsToArray(data);
            if (userIds.length == 0) {
                console.log('notify receiver users undefined');
                return true;
            }

            console.log('notify inserted db is ' + socket.dbKey);
            // var notifyId =mongoDB ? nowDate : insertNotifyToMysql(data, socket.dbKey);
            var notifyIdNew = insertNotifyToMysql(data, socket.dbKey);
            var notifyId = mongoDB ? nowDate : notifyIdNew;
            console.log('notifyId', notifyId);
            // save notify to database
            Promise.all([userIds, notifyId])
                .then(values => {
                    userIds = values[0];
                    notifyId = values[1];
                    debug(userIds);
                    debug(notifyId);
                    data.uIcon = data.userIcon;
                    data.notifyId = notifyId;
                    data.sendDate = Math.floor(nowDate / 1000);

                    // mass message илгээж байгаа бол recent users шинэчилнэ
                    if (typeof data.users == 'object' && data.type == 'mass') {
                        recentUsersUpdate(
                            socket.dbKey,
                            data.userId,
                            'mass',
                            'mass',
                            userIds.slice(0, 5)
                        );
                    }

                    // receiver users-ийг database рүү бичилт хийнэ
                    if (mongoDB) {
                        var d = {
                            id: nowDate,
                            pro: data.pro,
                            mod: data.mod,
                            type: data.type,
                            itm_id: data.itemId,
                            head: escape(data.headText),
                            tle: escape(data.title),
                            cont: escape(data.content),
                            url: escape(data.url),
                            s_id: data.userId,
                            s_nme: data.userName,
                            s_icn: data.userIcon,
                            sent_dt: data.sendDate,
                            seen_dt: 0,
                            is_new: 1,
                        };
                        userIds.forEach(id =>
                            mongoDB.notifies_i.update(
                                { user_id: id },
                                // { $addToSet: { items: d } }
                                {
                                    $push: {
                                        items: {
                                            $each: [d],
                                            $sort: { sendDate: 1 },
                                            $slice: -400,
                                        },
                                    },
                                },
                                {
                                    upsert: true,
                                },
                                function (err, docs) {
                                    debug(err);
                                    debug(docs, d);
                                }
                            )
                        );
                    } else if (userIds.length > 0) {
                        mysql.cloud
                            .getConnection()
                            .then(conn => {
                                qry =
                                    'INSERT INTO `' +
                                    socket.dbKey +
                                    '`.`notify_byuser_k`(`notify_id`,`user_id`,`type`,`seen_date`) ' +
                                    'SELECT ' +
                                    mysql.escape(notifyId) +
                                    ",`id`,'0'," +
                                    data.sendDate +
                                    ' FROM ' +
                                    '`cloud_hr`.`_able_worker_i` WHERE `id` IN (' +
                                    userIds.join(',') +
                                    ')';
                                // debug(qry);
                                var result = conn.query(qry);
                                //conn.release();
                                mysql.cloud.releaseConnection(conn);
                                return result;
                            })
                            .then(res => {
                                console.log(res.affectedRows + ' users inserted');
                            })
                            .catch(err => {
                                console.error("Error in socket.on('notify'):", qry, err);
                            });
                    }

                    // emit to others
                    if (data.hidden) {
                        delete data.userId;
                        delete data.comId;
                        delete data.userName;
                        delete data.userApp;
                        delete data.userCom;
                        delete data.userIcon;
                        delete data.uIcon;
                    }
                    if (typeof data.users == 'object') {
                        userIds.forEach(id => io.to(id).emit('notify', data));
                    } else {
                        io.to('com' + data.comId).emit('notify', data);
                    }

                    // mysql connection close
                    // userIds.forEach(id => {
                    //     mysql.cloud
                    //         .getConnection()
                    //         .then(conn => {
                    //             qry =
                    //                 'REPLACE INTO `' +
                    //                 socket.dbKey +
                    //                 '`.`news_feed_i` ' +
                    //                 '(`user_id`,`pro`,`mod`,`item_id`,`sender_id`,`date`,`title`) ' +
                    //                 'VALUES(?,?,?,?,?,?,?)';
                    //             var para = [
                    //                 id,
                    //                 data.pro,
                    //                 data.mod,
                    //                 data.itemId,
                    //                 data.userId,
                    //                 data.sendDate,
                    //                 data.headText,
                    //             ];
                    //             qry = mysql.format(qry, para);
                    //             var result = conn.query(qry);
                    //             //conn.release();
                    //             mysql.cloud.releaseConnection(conn);
                    //             return result;
                    //         })
                    //         .catch(err => {
                    //             console.error("Error in socket.on('notify'):", qry, err);
                    //         });
                    // });

                    // if (userIds.length > 0) {
                    //     mysql.cloud
                    //         .getConnection()
                    //         .then(conn => {
                    //             qry =
                    //                 'DELETE FROM `' +
                    //                 socket.dbKey +
                    //                 '`.`news_feed_i` ' +
                    //                 'WHERE `user_id` NOT IN (?) AND `pro`=? AND `mod`=? AND `item_id`=?';
                    //             var para = [userIds, data.pro, data.mod, data.itemId];
                    //             qry = mysql.format(qry, para);
                    //             console.log(qry);
                    //             try {
                                    
                    //             } catch (error) {
                                    
                    //             }
                    //             var result = conn.query(qry);
                    //             //conn.release();
                    //             mysql.cloud.releaseConnection(conn);
                    //             return result;
                    //         })
                    //         .catch(err => {
                    //             console.error("Error in socket.on('notify'):", qry, err);
                    //         });
                    // }
                })
                .catch(reason => {
                    console.error("Error in socket.on('notify'):", qry, reason);
                });
        });

        socket.on('massmsg', data => {
            var time = new Date().getTime();
            data.notifyId = time;
            data.sendDate = parseInt(new Date().getTime() / 1000);
            console.log(data);

            if (data.shareMode == 1) {
                if (data.share == 1) {
                    // хэрэглэгч сонгоод явуулахад
                    console.log('\nMass msg to selected users.');
                    if (typeof data.users == 'object') {
                        var userIds = userIdsToArray(data);
                        debug(userIds);
                        sendMass(userIds, data, socket);
                    }
                } else if (data.share == 2) {
                    // Байгууллага сонгоод явуулахад
                    if (typeof data.comIds != 'object') return;
                    var comIds = [];
                    Object.keys(data.comIds).forEach(i => {
                        if (typeof data.comIds[i] == 'string')
                            data.comIds[i] = parseInt(data.comIds[i]);

                        if (typeof data.comIds[i] != 'number') return;
                        if (data.comIds[i] < 1) return;

                        comIds.push(mysql.escape(data.comIds[i]));
                    });
                    if (comIds.length > 0) {
                        debug(comIds.join());
                        console.log('\nMass msg to other com users.');
                        sendMassToCom(data, socket, comIds.join());
                    }
                } else if (data.share == 3) {
                    // Бусад бүх байгууллага руу
                    console.log('\nMass msg to all com.');
                    getRootComId(socket.dbKey, socket.comId).then(rootComId => {
                        debug(rootComId);
                        sendMassToAll(data, socket, mysql.escape(rootComId));
                    });
                }
            } else if (data.shareMode == 2) {
                // байгууллага дотроо
                console.log('\nMass msg to my com users.');
                sendMassToCom(data, socket, mysql.escape(data.comId));
            }
        });

        socket.on('windowMass', (to, data) => {
            var time = new Date().getTime();
           // data.notifyId = time;
            //data.sendDate = parseInt(new Date().getTime() / 1000);
            console.log('windowMass', data);
            if (typeof data === 'string') {
                data = JSON.parse(data);
            }
            if (data.comId == undefined) return;
            if (data.comId != '') {
                let coms =data.comId.split(',');
                coms.forEach(comId => {
                    console.log(comId);
                    sendMassWindow(data, socket, comId*1);
                });

            };
        });


        socket.on('openChatWin', userId => {
            console.log('openChatWin #myHID:' + socket.userId + ' #openID:' + userId);
            if (isDesktopLoggedIn(socket.userId))
                io.to('desktop' + socket.userId).emit('openChatWin', userId);
        });

        socket.on('userStatus', data => {
            console.log('socket.on.userStatus');
            debug('socket.on.userStatus', data);
            data.startDate = Date.now();
            emitToOthers(socket, 'userStatus', data);

            if (data.status === 'away') {
                redis.set('idle' + data.userId, Math.floor(data.startDate / 1000));
            } else {
                redis.del('idle' + data.userId);
            }
        });

        socket.on('checkDesktop', data => {
            socket.emit('checkDesktop', isDesktopLoggedIn(socket.userId));
        });

        socket.on('xmlSend', xmlInfo => {
            io.to('desktop' + socket.userId).emit('xmlSend', xmlInfo);
        });

        socket.on('openMeeting', xmlInfo => {
            io.to('desktop' + socket.userId).emit('openMeeting', xmlInfo);
        });

        socket.on('xmlReceived', xml => {
            console.log('socket xml received');
            console.log(xml);
            io.to('web' + socket.userId).emit('xmlReceived', xml);
        });

        socket.on('getConnectedTokens', () => {
            console.log('getConnectedToken called');
            var mySockets = io.sockets.clients(socket.userId).map(sock => sock.token);
            console.log('fffffffffffffffff' + mySockets);
            socket.emit('getConnectedTokens', mySockets);
        });

        // this function only use for logout browser tabs
        socket.on('logout', () => {
            if (socket.isWeb) {
                io.to(socket.userId).emit('logout', true);
            }
        });
    };
};
