/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
* File Name   : auth.js
* Created at  : 2019-09-30
* Updated at  : 2019-09-30
* Author      : Boorch
* Purpose     :
* Description :
_._._._._._._._._._._._._._._._._._._._._.*/

'use strict';
// const debug  = require('debug')('able:auth');
const jwt = require('jsonwebtoken');
const { session } = require('../config');

module.exports = (db, is_next) => {
    const { redis } = db;

    return async (req, res, next) => {
        const return_value = {
            allow: false,
        };

        const accessToken = req.headers['x-access-token'] || req.query.accessToken; // || req.cookies.accessToken || req.query.accessToken;

        if (!accessToken) {
            // console.error('undefined accessToken');
            return_value.status = 'no_access_token';
            if (is_next) next(new Error(return_value.status));
            else res.json(return_value);
            return false;
        }

        try {
            const tokenData = jwt.verify(accessToken, session.auth_key);
            //console.log('data token');
            //console.log(tokenData);
            // debug(tokenData);
            if (!tokenData.myHID) {
                console.error('undefined user_id');
                return_value.status = 'bad_token';

                if (is_next) next(new Error(return_value.status));
                else res.json(return_value);
                return false;
            }

            // console.log('successfully checked user: '+ tokenData.uname);
            return_value.data = tokenData;
            return_value.allow = true;
            return_value.status = 'successfully';
            if (is_next) next();
            else res.json(return_value);
        } catch (err) {
            console.error('token not verified');
            return_value.status = 'token_not_match';
            if (is_next) next(new Error(return_value.status));
            else res.json(return_value);
        }
    };
};
