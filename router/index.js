/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
* File Name   : index.js
* Created at  : 2019-09-27
* Updated at  : 2019-09-27
* Author      : Boorch
* Purpose     : 
* Description : 
_._._._._._._._._._._._._._._._._._._._._.*/

"use strict";

module.exports = (app, db, io) => {
	app.set('db', db);
	app.set('io', io);
	
	app.use('/redisData', require('./redisData/_redisData'));
	app.use('/notify', require('./auth')(db, true), require('./notify/_notify'));
	app.get('/favicon.ico', (req, res) => {
		res.status(204).json({nope: true});
	});

	app.use((req, res) => {
	//	console.log(req, 'req');
		res.status(500);
		res.json({
			msg   : 'No matched router',
			error : true
		});
	});
	
}
