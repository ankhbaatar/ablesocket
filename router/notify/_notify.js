/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
* File Name   : tree_i.js
* Created at  : 2019-12-04
* Updated at  : 2019-12-04
* Author      : Boorch
* Purpose     : 
* Description : mysql_cloudHRCenter:localhost:8000
_._._._._._._._._._._._._._._._._._._._._.*/

"use strict";

const express = require('express');
const router = express.Router();

const notifyFunction = require('../../src/notify/notify.js');
router.post('/creat', async(req, res) => {
	res.json(await notifyFunction.receivedNotify(req));
})

module.exports = router;
