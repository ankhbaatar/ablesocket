/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
* File Name   : tree_i.js
* Created at  : 2019-12-04
* Updated at  : 2019-12-04
* Author      : Boorch
* Purpose     : 
* Description : mysql_cloudHRCenter:localhost:8000
_._._._._._._._._._._._._._._._._._._._._.*/

"use strict";

const express = require('express');
const router = express.Router();

router.get('/byKey', async(req, res) => {
	const db = req.app.get('db');
	const key = req.query.redisKey?req.query.redisKey:'';
	const list = await db.redis.get(key);

	
	res.json({
		resisData:list
	});
})

module.exports = router;
