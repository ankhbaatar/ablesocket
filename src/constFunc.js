const mysql = require('../module/_mysql');
const fs = require('fs');
const config = require("../config.js");

function encStr(str) {
    let value = new Buffer.from(str);
    
    let value1 = value.toString('base64');
    let len= value1.length;
	let a=Math.ceil(len/3);
	let b=Math.ceil(len/4);
	let cut1= value1.substr(a,1);
    let cut2= value1.substr(b,1);

    let paste1 = replaceAt(value1, b, cut1);
    let paste2 = replaceAt(paste1, a, cut2);
    return paste2;
}

function replaceAt(str, index, ch) {
    return str.replace(/./g, (c, i) => i == index ? ch : c)
}

function paginate(array, page_size, page_number) { // array, limit , page
    // human-readable page numbers usually start with 1, so we reduce 1 in the first argument
    return array.slice((page_number - 1) * page_size, page_number * page_size);
}

function getImageType(type) {
    switch(type)  {
        case "image/gif": return true;
        case "image/pjpeg":  return true;
        case "image/jpeg":  return true;
        case "image/jpg":  return true;
        case "image/png":  return true;
        case "image/x-png":  return true;
        default: return false
    }
}

async function getUserInformation(userId) {
    try {
        const conn =await mysql.cloud.getConnection();
        var cloudDB = (config.mysql.cloudDB) ? config.mysql.cloudDB : 'cloudCenter';
        var cloudHRDB = (config.mysql.cloudHRDB) ? config.mysql.cloudHRDB : 'cloudHRCenter';
        var result = await conn.query("SELECT W.`id`, W.`system_name`, W.`com_name`, W.`dep_name`, W.`app_name`, W.`photo`, U.`user_icon`, U.`email`, U.`personal_mobile` FROM `"+cloudHRDB+"`.`_able_worker_i` AS W LEFT JOIN `"+cloudDB+"`.`_user_i` AS U ON W.user_id = U.user_id WHERE W.`id`=?",[userId]);
        conn.release();
        return result[0];
    }
    catch(e) {
        console.log(e);
        return {'error': 'Not found'};
    }
}

function getUserInfo(userId,redis) {
    // хэрэв redis cache-д хэрэлэгчийн мэдээлэл байвал авна
    // байхгүй бол mysql-ээс авна.
    // _able_worker_i
// console.log("userId", userId);
    return redis.get("_able_worker_i-"+ userId)
    .then(res => {
        if(!res)  {
            return mysql.cloud_hr.getConnection()
            .then(conn => {
                var sql = "SELECT * FROM ?? WHERE ?? = ?";
                var para = ['_able_worker_i', 'id', userId];
                sql = mysql.format(sql, para);
                var result = conn.query(sql);
                //conn.release();
                mysql.cloud_hr.releaseConnection(conn);
                return result;
            })
            .then(rows => {
                redis.set("_able_worker_i-"+ userId, rows[0]);
                return rows[0];
            })
            .catch((err) => {
                console.error('Error in getUserInfo:', err);
            });
        }
        return res;
    })
    .then(wRow => {
        var humanId =wRow['user_id'];

        // _user_i
        var uPromise =redis.get("_user_i-"+ userId)
        .then(res => {
            if(!res)  {
                return mysql.cloud.getConnection()
                .then(conn => {
                    var sql = "SELECT * FROM ?? WHERE ?? = ?";
                    var para = ['_user_i', 'user_id', humanId];
                    sql = mysql.format(sql, para);
                    var result = conn.query(sql);
                    mysql.cloud.releaseConnection(conn);
                    //conn.release();
                    return result;
                })
                .then(rows => {
                    redis.set("_user_i-"+ userId, rows[0]);
                    return rows[0];
                })
                .catch((err) => {
                    console.error('Error in getUserInfo:', err);
                });
            }
            return res;
        })
        .catch((err) => {
            console.error('Error in getUserInfo:', err);
        });

        // _able_human_i
        var hPromise =redis.get("_able_human_i-"+ userId)
        .then(res => {
            // debug('redis res:',res);
            if(!res)  {
                return mysql.cloud_hr.getConnection()
                .then(conn => {
                    var sql = "SELECT * FROM ?? WHERE ?? = ?";
                    var para = ['_able_human_i', 'id', humanId];
                    sql = mysql.format(sql, para);
                    var result = conn.query(sql);
                    mysql.cloud_hr.releaseConnection(conn);
                    //conn.release();
                    return result;
                })
                .then(rows => {
                    redis.set("_able_human_i-"+ userId, rows[0]);
                    // debug('mysql res:',rows[0]);
                    return rows[0];
                })
                .catch((err) => {
                    console.error('Error in getUserInfo:', err);
                });
            }
            return res;
        })
        .catch((err) => {
            console.error('Error in getUserInfo:', err);
        });

        // _user_status_i
        var uStatusPromise =redis.get("_user_status_i-"+ userId)
        .then(res => {
            // debug('redis res:',res);
            if(!res)  {
                return mysql.cloud.getConnection()
                .then(conn => {
                    var sql = "SELECT * FROM ?? WHERE ?? = 1 AND ?? = ?";
                    var para = ['_user_status_i', 'is_active','user_id', userId];
                    sql = mysql.format(sql, para);
                    var result = conn.query(sql);
                    mysql.cloud.releaseConnection(conn);
                    //conn.release();
                    return result;
                })
                .then(rows => {
                    if(rows) redis.set("_user_status_i-"+ userId, rows[0]);
                    // debug('mysql res:',rows[0]);
                    return rows[0];
                })
                .catch((err) => {
                    console.error('Error in getUserInfo:', err);
                });
            }
            return res;
        })
        .catch((err) => {
            console.error('Error in getUserInfo:', err);
        });

        return Promise.all([uPromise, hPromise, uStatusPromise]).then(value => {
            return {
                wRow: wRow,
                uRow: value[0],
                hRow: value[1],
                sRow: value[2],
            }
        });
    })
    .catch((err) => {
        console.error('Error in getUserInfo:', err);
    });
}

function mkDir(path) {
    if (!fs.existsSync(path)){
        fs.mkdirSync(path);
    }
}

function fileExist(path) {
    fs.unlink(path, (err) => {
        if (err) return false;
        else return true;
    });
}

function OnlineRoom(users) {
    var resultUsers =  users.filter(function(user) {
        return user.isOnline == true;
    });
    return resultUsers.length > 1 ? true : false;
}

module.exports = {
    encStr,
    paginate,
    getImageType,
    getUserInfo,
    getUserInformation,
    mkDir,
    fileExist,
    OnlineRoom
}

