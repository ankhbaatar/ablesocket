/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
* File Name   : tree_i.js
* Created at  : 2019-12-04
* Updated at  : 2019-12-04
* Author      : Sodoo
* Purpose     : 
* Description : mysql_cloudHRCenter:localhost:8000
_._._._._._._._._._._._._._._._._._._._._.*/

"use strict";
// const mysql = require('../../module/_mysql');

function replaceNotifyEnd(txt)  {
    // console.log("txt", txt);
    if(txt =='')  return txt;
    if(txt.search('шилжиж ирлээ') > 0)  return txt.replace('шилжиж ирлээ','шилжүүллээ');
    if(txt.search('ирлээ') > 0)  return txt.replace('ирлээ','илгээлээ');
    if(txt.search('шинэчлэгдлээ') > 0)  return txt.replace('шинэчлэгдлээ','шинэчиллээ');
    if(txt.search('нэмэгдлээ') > 0)  return txt.replace('нэмэгдлээ','нэмлээ');
    if(txt.search('хянагдлаа') > 0)  return txt.replace('хянагдлаа','хяналаа');
    if(txt.search('гүйцэтгэгдлээ') > 0)  return txt.replace('гүйцэтгэгдлээ','гүйцэтгэлээ');
    return txt;
}

async function receivedNotify(data) {
	let db = data.app.get('db');
	let mysql = db.mysql;
	let io = data.app.get('io');
	let reqdata =data.body,
		qry,  nowDate =Date.now(),
		userIds =userIdsToArray(reqdata);

	if(userIds.length == 0)  {
		console.log('notify receiver users undefined');
		return true;
	}

	let itemId= mysql.escape(reqdata.itemId),
		content= mysql.escape(reqdata.content),
		headText= mysql.escape(reqdata.headText),
		title= mysql.escape(reqdata.title),
		pro= mysql.escape(reqdata.pro),
		mod= mysql.escape(reqdata.mod),
		type= mysql.escape(reqdata.type),
		url= mysql.escape(reqdata.url),
		userId= mysql.escape(reqdata.userId),
		userIcon= mysql.escape(reqdata.userIcon),
		userName= mysql.escape(reqdata.userName);

	let fields ='`item_id`,`content`,`headText`,`title`,`pro`,`mod`,`type`,`url`,`sender_id`,`sender_name`,`sender_icon`,`sent_date`',
		value =itemId +","+ `${content}` +","+ `${replaceNotifyEnd(headText)}` +","+ 
		`${title}` +","+ `${pro}` +","+ `${mod}` +","+ 
		`${type}` +","+ `${url}`+"," + userId +","+
		`${userName}` +","+ `${userIcon}` +","+ Date.now();

		return mysql.cloud.getConnection()
		.then( async conn => {
			qry = "INSERT INTO `"+ reqdata.dbKey +"`.`notify_i` ("+ fields +") VALUES("+ value +")";
			let nresult =await conn.query(qry),
				notifyId =nresult.insertId;
			mysql.cloud.releaseConnection(conn);
			Promise.all([userIds, notifyId]).then(values => {
				userIds =values[0];
				notifyId =values[1];
				reqdata.uIcon =reqdata.userIcon;
				reqdata.notifyId =notifyId;
				reqdata.sendDate =Math.floor(nowDate / 1000);

				// receiver users-ийг database рүү бичилт хийнэ
				if(userIds.length > 0)  {
					mysql.cloud.getConnection()
					.then(conn => {
						qry = "INSERT INTO `"+ reqdata.dbKey +"`.`notify_byuser_k`(`notify_id`,`user_id`,`type`,`seen_date`) "+
							  "SELECT "+ mysql.escape(notifyId) +",`id`,'0',"+ reqdata.sendDate +" FROM "+
							  "`cloud_hr`.`_able_worker_i` WHERE `id` IN ("+ userIds.join(',') +")";
						let uresult =conn.query(qry);
						mysql.cloud.releaseConnection(conn);

						return uresult;
					})
					.then(res => {
						console.log(res.affectedRows +" users inserted");
					})
					.catch(err => {
						console.error('Error in socket.on(\'notify\'):', qry, err);
					})
				}

				// emit to others
				if(reqdata.hidden)  {
					delete reqdata.userId;
					delete reqdata.comId;
					delete reqdata.userName;
					delete reqdata.userApp;
					delete reqdata.userCom;
					delete reqdata.userIcon;
					delete reqdata.uIcon;
				}
				if(typeof reqdata.users == 'object')  {
					// console.log("forEach",userIds);
					userIds.forEach(id => io.to(id).emit('notify', reqdata));
				}
				else  {
					io.to('com'+ reqdata.comId).emit('notify', reqdata);
				}
			})
			.catch(err => {
				console.error('Error in socket.on(\'notify\'):', err);
			});

			return nresult;
		})
		.then(res => {
			return res.insertId;
		})
		.catch(err => console.error('Error in insertNotifyToMysql:', err))
}

async function userIdsToArray(data) {
	if(typeof data.users =='object')  {
		// Хэрэглэгчдийн жагсаалт
		var userIds =[];
		Object.keys(data.users).forEach(i => {
			if(typeof data.users[i] == 'string')  data.users[i] =parseInt(data.users[i]);
			if(typeof data.users[i] != 'number')  return;
			if(data.users[i] < 1)  return;
			userIds.push(data.users[i]);
		});
		data.users =userIds;
		return userIds;
	}
}

module.exports = {
    receivedNotify
}
